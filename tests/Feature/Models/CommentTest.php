<?php

namespace Tests\Feature\Models;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Model;
class CommentTest extends TestCase
{

    use RefreshDatabase,ModelHelperTesting;

    protected function model(): Model
    {
        return new Comment();
    }
    /**
     * A basic feature test example.
     */


    public function testCommentRelationShipWithPost()
    {
        $comment=Comment::factory()
            ->hasCommentable(Post::factory())
            ->create();
        $this->assertTrue($comment->commentable instanceof Post);


    }

    public function testCommentRelationShipWithUser()
    {
        $comment=Comment::factory()
            ->for(User::factory())
            ->create();
        $this->assertTrue($comment->user instanceof User);
        $this->assertTrue(isset($comment->user->id));

    }
}
