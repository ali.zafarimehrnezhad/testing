<?php

namespace Tests\Feature\Models;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Model;

class UserTest extends TestCase
{

    use RefreshDatabase,ModelHelperTesting;


    /**
     * A basic feature test example.
     */

    public function testUserRelationShipWithPost()
    {
        $count= rand(1,10);
        $user=User::factory()
            ->hasPosts($count)
            ->create();

        $this->assertTrue($user->posts->first() instanceof Post);
        $this->assertCount($count,$user->posts);

    }

    public function testUserRelationShipWithComment()
    {
        $count=rand(1,10);
        $user=User::factory()
            ->hasComments($count)
            ->create();
        $this->assertCount($count,$user->comments);
        $this->assertTrue($user->comments->first() instanceof Comment);

    }

    protected function model(): Model
    {
        return new User();
    }
}
