<?php

namespace Tests\Feature\Models;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Model;

class TagTest extends TestCase
{

    use RefreshDatabase,ModelHelperTesting;
    protected function model(): Model
    {
        return new Tag();
    }


    public function testTagRelationShipWithPost()
    {
        $count= rand(1,10);
        $tag=Tag::factory()
            ->hasPosts($count)
            ->create();
        $this->assertTrue($tag->posts->first() instanceof Post);
        $this->assertCount($count,$tag->posts);

    }
}
